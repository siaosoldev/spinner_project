
-- menu.lua
local storyboard = require "storyboard"
local widget = require("widget")

local scene = storyboard.newScene() 
local screenGroup,rect,paint
local _W = display.contentWidth
local _H = display.contentHeight
local myTimer,mySpinner


function scene:createScene(event)
   screenGroup = self.view

   paint = {
    type = "gradient",
    --color1 = { 1, 0, 0.4 },
    --color2 = { 1, 0, 0, 0.2 },
    color1 = { 5.8, 1.9, 2.4 },
    color2 = { 1, 0, 0, 5.2 },
    direction = "down"
    }

    rect = display.newRect( 0, 0, _W, _H )
    rect.x = _W*.5
    rect.y = _H*.5
    rect.fill = paint
    screenGroup:insert(rect)

end

function scene:enterScene(event)
	screenGroup = self.view

    storyboard.purgeAll( "lua.game" )
    storyboard.removeAll( "lua.game" )

    local text = display.newText("reload...",0,0,nil,35)
    text.x = _W*.5
    text.y = _H*.40
    screenGroup:insert(text)

    mySpinner = widget.newSpinner
    {
        left = _W/2,
        top =  _H/2,
        time = 1000
    }
    mySpinner:scale(3,3)
    screenGroup:insert(mySpinner)
    mySpinner:start()
    
    local changeScene = function()          
        storyboard.gotoScene( "lua.menu", "fade", 100 )
        return true
        
    end
    myTimer = timer.performWithDelay( 1000, changeScene, 1 )

end

function scene:exitScene(event)
    -- remove all display object here..
    screenGroup:removeSelf()
    screenGroup = nil

    if myTimer then timer.cancel( myTimer );
    end
    
    if mySpinner then
        mySpinner:removeSelf() 
        mySpinner  = nil
    end

end

function scene:destroyScene(event)  

end

function scene:overlayBegan(event)
    --transition.to(group.scene,{time = 500,alpha = 0.3})
end

function scene:overlayEnded(event)
    --transition.to(group.scene,{time = 500,alpha = 1})
    
end
scene:addEventListener( "createScene"   ,scene )
scene:addEventListener( "enterScene"    ,scene )
scene:addEventListener( "exitScene"     ,scene )
scene:addEventListener( "destroyScene"  ,scene )
scene:addEventListener( "overlayBegan"  ,scene )
scene:addEventListener( "overlayEnded"  ,scene )
return scene
