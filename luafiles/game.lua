
-- menu.lua
local storyboard = require "storyboard"
local widget = require("widget")
local Particles = require("luafiles.lib_particle_candy")

local scene = storyboard.newScene()

local _W= display.contentWidth
local _H= display.contentHeight

local onEnterFrame
local Sample = {}
local Tmp

function scene:createScene(event)
   screenGroup = self.view

   --local bg = display.newImageRect("images/background_space2.jpg",_W,_H)--
   --bg.x = _W*.5
   --bg.y = _H*.5
   --screenGroup:insert(bg)

end


function scene:enterScene(event)
    --storyboard.purgeAll()  --remove if Overlay
    --storyboard.removeAll() --remove if Overlay

--[[
  Particles.CreateEmitter("E1", display.contentWidth*.5,display.contentHeight*.5, 0, false, true)

    Particles.CreateParticleType ("Rings", 
    {
        imagePath          = "water_ring.png",
        imageWidth         = 128,   
        imageHeight        = 128,   
        velocityStart      = 0,     
        alphaStart         = 0.75,  
        fadeInSpeed        = 0.0,   
        fadeOutSpeed       = -0.5,  
        fadeOutDelay       = 0,     
        scaleStart         = 0.1,   
        scaleVariation     = 0.1,   
        scaleInSpeed       = 2.0,   
        weight             = 0.0,   
        useEmitterRotation = false, 
        emissionShape      = 3,     
        emissionRadius     = 400,   
        lifeTime           = 4000,      
        blendMode          = "add",     
    } )


    Particles.AttachParticleType("E1", "Rings"   , 3, 5000, 1000) 

    Particles.SetEmitterListener("E1", function(event)
    end)

    Particles.StartEmitter("E1")
    function onEnterFrame(event)
        Particles.Update()
    end

    Runtime:addEventListener("enterFrame", onEnterFrame)
   --]]

   -- CREATE GROUP
    Sample.Group   = display.newGroup()
    Sample.Group.x = centerX
    Sample.Group.y = centerY
    --screenGroup:insert(Sample.Group)
    display.getCurrentStage():insert(2,Sample.Group)

    -- BG 
    Tmp  = display.newImage("images/background_space2.jpg")
    Tmp.xScale = (deviceW*1.18) / Tmp.width
    Tmp.yScale = Tmp.xScale
    Tmp.x      = 0
    Tmp.y      = 0
    Sample.Group:insert(Tmp)


    -- CREATE EMITTERS (NAME, SCREENW, SCREENH, ROTATION, ISVISIBLE, LOOP)
    Particles.CreateEmitter("E1", display.contentWidth*.5,display.contentHeight*.5, 0, false, true)
    
    -- DEFINE PARTICLE TYPES
    Particles.CreateParticleType ("Clouds1", 
        {
        imagePath         = "images/cloud9.png",
        imageWidth        = 256,    
        imageHeight       = 256,    
        velocityStart     = 150,    
        killOutsideScreen = false,  
        lifeTime          = 2500,  
        alphaStart        = 0,      
        fadeInSpeed       = 1,  
        fadeOutSpeed      = -0.85,  
        fadeOutDelay      = 1000,   
        scaleStart        = 3.0,    
        scaleVariation    = 0.5,
        scaleOutSpeed     = -1.2,   
        scaleOutDelay     = 1,      
        emissionShape     = 2,      
        emissionRadius    = 300,    
        faceEmitter       = true,
        blendMode         = "screen",
        colorStart        = {.9,.9,1},
        colorChange       = {-.23,-.23,-.15},
        } )

    Particles.CreateParticleType ("Clouds2", 
        {
        imagePath         = "images/cloud10.png",
        imageWidth        = 256,    
        imageHeight       = 256,    
        velocityStart     = 150,    
        killOutsideScreen = false,  
        lifeTime          = 2500,  
        alphaStart        = 0,      
        fadeInSpeed       = 1,  
        fadeOutSpeed      = -0.85,  
        fadeOutDelay      = 1000,   
        scaleStart        = 3.0,    
        scaleVariation    = 0.5,
        scaleOutSpeed     = -1.2,   
        scaleOutDelay     = 1,      
        emissionShape     = 2,      
        emissionRadius    = 300,    
        faceEmitter       = true,
        blendMode         = "screen",
        colorStart        = {.86,.58,1},
        colorChange       = {-.19,-.39,-.31},
        } )

    Particles.CreateParticleType ("Stars", 
        {
        imagePath         = "images/starcloud.png",
        imageWidth        = 256,    
        imageHeight       = 256,    
        velocityStart     = 150,    
        killOutsideScreen = false,  
        lifeTime          = 2500,  
        alphaStart        = 0,      
        fadeInSpeed       = 1,  
        fadeOutSpeed      = -0.5,   
        fadeOutDelay      = 1000,   
        scaleStart        = 3.0,    
        scaleVariation    = 0.5,
        scaleOutSpeed     = -1.2,   
        scaleOutDelay     = 1,      
        emissionShape     = 2,      
        emissionRadius    = 300,    
        faceEmitter       = true,   
        blendMode         = "screen",
        colorStart        = {1,.78,1},
        colorChange       = {-.27,-.27,0},
        } )

    -- FEED EMITTERS (EMITTER NAME, PARTICLE TYPE NAME, EMISSION RATE, DURATION, DELAY)
    Particles.AttachParticleType("E1", "Clouds1", 5, 99999,0) 
    Particles.AttachParticleType("E1", "Clouds2", 5, 99999,0) 
    Particles.AttachParticleType("E1", "Stars"  , 5, 99999,0) 

    -- TRIGGER THE EMITTERS
    Particles.StartEmitter("E1")

    function onEnterFrame(event)
        -- ROTATE ALL PARTICLES
        Sample.Group.rotation = math.sin(system.getTimer()/2500)*180
        Particles.Update()
    end

    -- DO ENTER FRAME STUFF
    Runtime:addEventListener("enterFrame", onEnterFrame)

end

function scene:exitScene(event)
   
    screenGroup:removeSelf()
    screenGroup = nil
end

function scene:destroyScene(event)  

end

function scene:overlayBegan(event)
    
end

function scene:overlayEnded(event)
    
end
scene:addEventListener( "createScene"   ,scene )
scene:addEventListener( "enterScene"    ,scene )
scene:addEventListener( "exitScene"     ,scene )
scene:addEventListener( "destroyScene"  ,scene )
scene:addEventListener( "overlayBegan"  ,scene )
scene:addEventListener( "overlayEnded"  ,scene )
return scene
--]]









