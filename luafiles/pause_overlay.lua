
-- menu.lua
local storyboard = require "storyboard"
local widget = require("widget")
local ads    =require("lua.ads")

local scene = storyboard.newScene() 
local screenGroup,rect,paint
local _W = display.contentWidth
local _H = display.contentHeight


function scene:createScene(event)
   screenGroup = self.view

   paint = {
    type = "gradient",
    --color1 = { 1, 0, 0.4 },
    --color2 = { 1, 0, 0, 0.2 },
    color1 = { 5.8, 1.9, 2.4 },
    color2 = { 1, 0, 0, 5.2 },
    direction = "down"
    }

    rect = display.newRect( 0, 0, _W, _H )
    rect.x = _W*.5
    rect.y = _H*.5
    rect.fill = paint
    screenGroup:insert(rect)

    resumeButton = widget.newButton {
            width = 90,
            height = 70,
            defaultFile = "images/refresh.png",
            overFile = "images/refresh-over.png",        
            onEvent = function(event)
            if "began" == event.phase then 
                ads.calltapfortap("banner_hide")  
                storyboard.hideOverlay("lua.game")
            end
                return true
            end
        }
        resumeButton.x = _W*.63
        resumeButton.y = _H*.5
        screenGroup:insert(resumeButton)

    exitButton = widget.newButton {
            width = 90,
            height = 70,
            defaultFile = "images/exit.png",
            overFile = "images/exit-over.png",        
            onEvent = function(event)
            if "began" == event.phase then
                 ads.calltapfortap("banner_hide")
                local options =
                {
                    effect = "fade",
                    time = 450,
                    --isModal=true
                } 
                storyboard.gotoScene("lua.reload_home",options)
            end
                return true
            end
        }
    exitButton.x = _W*.43
    exitButton.y = _H*.5
    screenGroup:insert(exitButton)  

end

function scene:enterScene(event)
	screenGroup = self.view
    --storyboard.purgeAll()  --remove if Overlay
    --storyboard.removeAll() --remove if Overlay

    local text = display.newText("PAUSE PAGE HERE.",0,0,nil,35)
    text.x = _W*.5
    text.y = _H*.25
    screenGroup:insert(text)
    ads.calltapfortap("banner_bottom")

end

function scene:exitScene(event)
    -- remove all display object here..
    screenGroup:removeSelf()
    screenGroup = nil

end

function scene:destroyScene(event)  

end

function scene:overlayBegan(event)
    --transition.to(group.scene,{time = 500,alpha = 0.3})
end

function scene:overlayEnded(event)
    --transition.to(group.scene,{time = 500,alpha = 1})
    
end
scene:addEventListener( "createScene"   ,scene )
scene:addEventListener( "enterScene"    ,scene )
scene:addEventListener( "exitScene"     ,scene )
scene:addEventListener( "destroyScene"  ,scene )
scene:addEventListener( "overlayBegan"  ,scene )
scene:addEventListener( "overlayEnded"  ,scene )
return scene
