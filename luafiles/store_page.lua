
-- menu.lua
local storyboard = require "storyboard"
local widget = require("widget")
local ads    =require("lua.ads")
local external=require("lua.external")


local scene = storyboard.newScene() 
local screenGroup,rect,paint
local _W = display.contentWidth
local _H = display.contentHeight

function scene:createScene(event)
   screenGroup = self.view

   paint = {
    type = "gradient",
    --color1 = { 1, 0, 0.4 },
    --color2 = { 1, 0, 0, 0.2 },
    color1 = { 5.8, 1.9, 2.4 },
    color2 = { 1, 0, 0, 5.2 },
    direction = "down"
    }

    rect = display.newRect( 0, 0, _W, _H )
    rect.x = _W*.5
    rect.y = _H*.5
    rect.fill = paint
    screenGroup:insert(rect)

    function Purchase_One( event )
        if "clicked" == event.action then
            local i = event.index
               if 1 == i then  
                ads.purchaseItem()
            elseif 2 == i then
                --ads.restorePurchases()
            end
        end
    end

    purcahse_oneCoins = widget.newButton
    {
        --defaultFile = "images/remove-ads.png",
        -- = "images/remove-ads-over.png",
        --id = "remove ads",
        width       = 120,
        height      = 70,
        label       = "Purchase One",
        onRelease = function(event)
            if event.phase == "ended" then
                --sound.effectPlay("button")
                print("In App")
                --ads.callflurry("Remove Ads")
               local alert = native.showAlert( "Purcahse Coins","BUY COINS?", { "BUY", "Cancel" }, Purchase_One )
            end
        end
    }
    purcahse_oneCoins.x = _W *.5
    purcahse_oneCoins.y = _H *.35
    screenGroup:insert(purcahse_oneCoins)

    function Purchase_Two( event )
        if "clicked" == event.action then
            local i = event.index
               if 1 == i then  
                ads.purchaseItem()
            elseif 2 == i then
                --ads.restorePurchases()
            end
        end
    end

    purcahse_twoCoins = widget.newButton
    {
        --defaultFile = "images/remove-ads.png",
        -- = "images/remove-ads-over.png",
        --id = "remove ads",
        width       = 120,
        height      = 70,
        label       = "Purchase Two",
        onRelease = function(event)
            if event.phase == "ended" then
                --sound.effectPlay("button")
                print("In App")
                --ads.callflurry("Remove Ads")
               local alert = native.showAlert( "Purcahse Coins","BUY COINS?", { "BUY", "Cancel" }, Purchase_One )
            end
        end
    }
    purcahse_twoCoins.x = _W *.5
    purcahse_twoCoins.y = _H *.55
    screenGroup:insert( purcahse_twoCoins)


        exitButton = widget.newButton {
            width = 80,
            height = 60,
            defaultFile = "images/exit.png",
            overFile = "images/exit-over.png",        
            onEvent = function(event)
            if "began" == event.phase then
         
                local options =
                {
                    effect = "zoomOutIn",
                    time = 300,
                    isModal=true
                } 
                 storyboard.hideOverlay("lua.menu",options)
            end
                return true
            end
        }
        exitButton.x = _W*.90
        exitButton.y = _H*.05
        screenGroup:insert(exitButton) 

end

function scene:enterScene(event)
	screenGroup = self.view
    --storyboard.purgeAll()  --remove if Overlay
    --storyboard.removeAll() --remove if Overlay

    local text = display.newText("~~~STORE~~~",0,0,nil,35)
    text.x = _W*.5
    text.y = _H*.15
    screenGroup:insert(text)

end

function scene:exitScene(event)
    -- remove all display object here..
    screenGroup:removeSelf()
    screenGroup = nil

end

function scene:destroyScene(event)  

end

function scene:overlayBegan(event)
    --transition.to(group.scene,{time = 500,alpha = 0.3})
end

function scene:overlayEnded(event)
    --transition.to(group.scene,{time = 500,alpha = 1})
    
end
scene:addEventListener( "createScene"   ,scene )
scene:addEventListener( "enterScene"    ,scene )
scene:addEventListener( "exitScene"     ,scene )
scene:addEventListener( "destroyScene"  ,scene )
scene:addEventListener( "overlayBegan"  ,scene )
scene:addEventListener( "overlayEnded"  ,scene )
return scene
