
-- menu.lua
local storyboard = require "storyboard"
local widget = require("widget")
--local ads    =require("luafiles.ads")
--local external=require("luafiles.external")

local scene = storyboard.newScene()
local _W = display.contentWidth
local _H = display.contentHeight

local screenGroup,rect,paint,startButton,removeAds,tap_navBar


function scene:createScene(event)
   screenGroup = self.view

   local bg = display.newImageRect("images/background_space2.jpg",_W,1000)--
   bg.x = _W*.5
   bg.y = _H*.5
   screenGroup:insert(bg)

    --score = display.newText("score: "..score,0,0,nil,35)
    --score:setTextColor (255/255,250/255,100/255)
    --score.x = _W*.5
    --score.y = _H*.65
    --screenGroup:insert(score)

    --hiscore = display.newText("high score: "..hiscore,0,0,nil,35)
    --hiscore:setTextColor (255/255,250/255,100/255)
    --hiscore.x = _W*.5
    --hiscore.y = _H*.75
    --screenGroup:insert(hiscore)

    startButton = widget.newButton {
            width = 220,
            height = 100,
            defaultFile = "images/play1.png",
            overFile = "images/play1-over.png",        
            onEvent = function(event)
            if "began" == event.phase then
                --tap_navBar.alpha=0
                --ads.calltapfortap("banner_hide")
                local options =
                {
                    effect = "fade",
                    time = 450,
                    --isModal=true
                } 
                 storyboard.gotoScene("luafiles.game",options)
            end
                return true
            end
        }
        startButton.x = _W*.5
        startButton.y = _H*.5
        screenGroup:insert(startButton)  

   
    -- Animate the start button. Scale up and down.
    local function startButtonAnimation( )
        local scaleUp = function( )
            startButtonTween = transition.to( startButton, { xScale=1.3, yScale=1.3, onComplete=startButtonAnimation } )
        end
            
        startButtonTween = transition.to( startButton, { xScale=0.9, yScale=0.9, onComplete=scaleUp } )
    end

    function anime_start()
        startButtonAnimation()
     end
    anime_time = timer.performWithDelay( 500, anime_start, 1 )

    function onComplete3( event )
        if "clicked" == event.action then
            local i = event.index
               if 1 == i then  
                --ads.purchaseItem()
            elseif 2 == i then
                --ads.restorePurchases()
            end
        end
    end

    --tap_navBar = display.newImageRect("images/top-navbar.png",1500,100)
    --tap_navBar.x=_W*0.50
    --tap_navBar.y=_H*0.03
    --tap_navBar.alpha=1
    --screenGroup:insert(tap_navBar)

    --if ads.remove == false then
    --    removeAds.alpha=0
    --end

    settings = widget.newButton
    {
        defaultFile = "images/settings.png",
        overFile = "images/settings-over.png",
        --id = "remove ads",
        width       = 75,
        height      = 55,
        onRelease = function(event)
            if event.phase == "ended" then
                --sound.effectPlay("button")
                --print("In App")
                --ads.callflurry("Remove Ads")
                local alert = native.showAlert( "APPS SETTINGS","MUSIC, SOUND EFFECTS ETC HERE..", { "Ok" })
            end
        end
    }
    settings.x = _W *.95
    settings.y = _H *.05
    screenGroup:insert(settings)

    how_to = widget.newButton
    {
        defaultFile = "images/how-to.png",
        overFile = "images/how-to-over.png",
        --id = "remove ads",
        width       = 75,
        height      = 55,
        onRelease = function(event)
            if event.phase == "ended" then
                --sound.effectPlay("button")
                --print("In App")
                --ads.callflurry("Remove Ads")
               local alert = native.showAlert( "HOW TO PLAY","TUTORIAL PAGE HERE..", { "Ok" })
            end
        end
    }
    how_to.x = _W *.85
    how_to.y = _H *.05
    screenGroup:insert(how_to)

    more_games = widget.newButton
    {
        defaultFile = "images/more-games.png",
        overFile = "images/more-games-over.png",
        --id = "remove ads",
        width       = 75,
        height      = 55,
        onRelease = function(event)
            if event.phase == "ended" then
                --sound.effectPlay("button")
                --print("In App")
                --ads.callflurry("Remove Ads")
               local alert = native.showAlert( "MORE GAMES","MORE GAME PAGE HERE..", { "Ok" })
            end
        end
    }
    more_games.x = _W *.75
    more_games.y = _H *.05
    screenGroup:insert(more_games)

    store = widget.newButton
    {
        defaultFile = "images/store.png",
        --overFile = "images/more-games-over.png",
        --id = "remove ads",
        width       = 75,
        height      = 55,
        onRelease = function(event)
            if event.phase == "ended" then
                --sound.effectPlay("button")
                --print("In App")
                --ads.callflurry("Remove Ads")
                --local alert = native.showAlert( "STORE","STORE PAGE HERE..", { "Ok" })
                local options =
                {
                    effect = "zoomOutIn",
                    time = 300,
                    isModal=true
                } 
                storyboard.showOverlay("lua.store_page",options)
            end
        end
    }
    store.x = _W *.65
    store.y = _H *.05
    store.alpha=0
    screenGroup:insert(store)


    local function onKeyEvent( event )      
    local phase = event.phase
    local keyName = event.keyName            
    if( (keyName == "back") and (phase == "down") ) then
       -- ads.callchartboost("fullscreen")
        local function onComplete( event )
            if "clicked" == event.action then
                local i = event.index
                if 1 == i then
                    --native.cancelAlert( alert ) 
                elseif 2 == i then
                    --ads.callchartboost("fullscreen")
                    ads.calltapfortap("fullscreen")
                    native.requestExit()
                end
            end
        end
        local alert = native.showAlert( "Exit", "Are you sure?", { "No", "Yes" }, onComplete )
        screenGroup:insert(alert);
        return true
    end         
    -- for default behavior, return false.
    return false            
   end


--[[ NOT AVAILABALE ON THIS VERSION

    --- CORONA ADS TEST
     -- Substitute your own placement IDs when generated
     local bannerPlacement = "top-banner-320x50"
     local interstitialPlacement = "interstitial-1"

     -- Corona Ads listener function
     local function adListener( event )

         -- Successful initialization of Corona Ads
         if ( event.phase == "init" ) then
             -- Show an ad
             coronaAds.show( bannerPlacement, false )
             --coronaAds.show( interstitialPlacement, true )
         end
     end

     -- Initialize Corona Ads (substitute your own API key when generated)
     coronaAds.init( "5223c2c3-cf81-4c43-ae41-2d4ed16552bc", adListener )
--]]


 ----
    function onComplete3( event )
        if "clicked" == event.action then
            local i = event.index
               if 1 == i then  
                --ads.purchaseItem()
            elseif 2 == i then
                --ads.restorePurchases()
            end
        end
    end

    removeAds = widget.newButton
    {
        defaultFile = "images/remove-ads.png",
        overFile = "images/remove-ads-over.png",
        --id = "remove ads",
        width       = 110,
        height      = 60,
        onRelease = function(event)
            if event.phase == "ended" then
                --sound.effectPlay("button")
                print("In App")
                --ads.callflurry("Remove Ads")
               local alert = native.showAlert( "Remove Ads","Remove the ads?", { "Remove", "Restore", "Cancel" }, onComplete3 )
            end
        end
    }
    removeAds.x = _W *.08
    removeAds.y = _H *.05
    screenGroup:insert(removeAds)

    --if ads.remove == false then
        --removeAds.alpha=0
    --end
   
    
    Runtime:addEventListener( "key", onKeyEvent );


end

function scene:enterScene(event)

    storyboard.purgeAll()  --remove if Overlay
    storyboard.removeAll() --remove if Overlay

    if(storyboard.getPrevious() ~= nil) then
    storyboard.purgeScene(storyboard.getPrevious())
    storyboard.removeScene(storyboard.getPrevious())

    -------ads.callchartboost("fullscreen")
    ads.calltapfortap("banner_bottom")
   
  end

end

function scene:exitScene(event)
    -- remove all display object here..
   
    screenGroup:removeSelf()
    screenGroup = nil

    if anime_time then
        timer.cancel(anime_time)
    end 
    --startButtonAnimation=false
end

function scene:destroyScene(event)  
end

function scene:overlayBegan(event)
    --transition.to(group.scene,{time = 500,alpha = 0.3})
end

function scene:overlayEnded(event)
    --transition.to(group.scene,{time = 500,alpha = 1}) 
end
scene:addEventListener( "createScene"   ,scene )
scene:addEventListener( "enterScene"    ,scene )
scene:addEventListener( "exitScene"     ,scene )
scene:addEventListener( "destroyScene"  ,scene )
scene:addEventListener( "overlayBegan"  ,scene )
scene:addEventListener( "overlayEnded"  ,scene )
return scene
