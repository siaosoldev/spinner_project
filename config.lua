--[[
if display.pixelHeight > 960 then -- for device with longer height e.g. iPhone 5 and most Android Devices.
    application = {
        content = {
        graphicsCompatibility = 1,  -- This turns on Graphics 1.0 Compatibility mode
            width = 640,
            height = 1136,
            scale = "zoomStretch", -- Stretch to fit entire screen. This will cause some objects to look flat or stretched.
            fps = 30,              -- But I advice to use this scale since some app stores rejects apps that will not display
            antialias = "true",    -- all objects properly.
        },
        imageSuffix = {
            ["@2x"] = 2,           -- For iPhone retina display. Apple requires that all apps support retina display.
        },
    }
else -- for devices with shorter height e.g. iPhone 4, 3GS, iPad, some Android phones.
    application = {
        content = {
        graphicsCompatibility = 1,  -- This turns on Graphics 1.0 Compatibility mode
            width = 640,
            height = 960,
            scale = "zoomStretch",
            fps = 30,
            antialias = "true",
        },
        imageSuffix = {
            ["@2x"] = 2,
        },
    }
end
--]]

--[[
application =
{
        content =
        {
         --graphicsCompatibility = 1,
               width = 640,
               height = 1136,
                scale = "zoomStretch",
                antialias = "true",
                fps = 30,
                
                imageSuffix =
                {
                    ["@2x"] = 2,
                },
        },
        license =
        {
        google =
            {
            --key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk34Ffx7oUYlL7SQHiqz9VeOOFILMMOV29xSeU94ZEBnVHFmPJtt/VNN3OWL7FdhQIVwU1vKtGmKKVwxtu6ln33Y3g6BeohjfCrFmxO8YS3s+txYkJNQmYOZv2hXEykbCCsH9hCQ9e5M82Eh8P2vxRCXD0ye6weUTVW1Z0ByjSOhymNF+N2EchPPM1DJuiGhFoa2UJfxMAhdc1glpb6w7dpBXdCGnyRSxWAg6ViNDqPu9Dl7JQgVqbT08SQWL15TJ3CYH/W7Oo5rug8TO8xpRdKoaYfmh881N+og2a9a57Da2CtFt16cADV96T86VUr2nZULGL4ODE5ofZ3O9VOEQJwIDAQAB",
                -- This is optional, it can be strict or serverManaged(default).
                -- stric WILL NOT cache the server response so if there is no network access then it will always return false.
                -- serverManaged WILL cache the server response so if there is no network access it can use the cached response.
            policy = "serverManaged", 
            },
       },
}
--]]

application = 
{
    content = 
    { 
    fps    = 60,
    width  = 480 * (display.pixelHeight/display.pixelWidth>1.5 and 1 or 1.5/(display.pixelHeight/display.pixelWidth)),
    height = 800 * (display.pixelHeight/display.pixelWidth<1.5 and 1 or (display.pixelHeight/display.pixelWidth)/1.5),
    scale  = "letterbox",
    xAlign = "center",
    yAlign = "center"
    }
}



